package com.luv2code.springdemo;

public class BasketballCoach implements Coach {
	
	private FortuneService fortuneService;		
	
	private String emailAddress;
	
	private String team;

	public BasketballCoach() {
		super();
	}	

	public BasketballCoach(FortuneService fortuneService) {
		super();
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {		
		return "The NBA is greatest";
	}

	@Override
	public String getDailyFortune() {		
		return "Raptors team " +  fortuneService.getFortune();
	}

	public void setFortuneService(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	public void setEmailAddress(String emailAddress) {
		System.out.println("We are in setter email");
		this.emailAddress = emailAddress;
	}

	public void setTeam(String team) {
		System.out.println("We are in setter team");
		this.team = team;
	}

	public FortuneService getFortuneService() {
		return fortuneService;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getTeam() {
		return team;
	}	

}
