package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanCycleLifeDemoApp {

	public static void main(String[] args) {
		//test scope
		
		//load
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanScopeCycleLife-applicationContext.xml");
		
		//retrieve bean container
		RugbyCoach coach = context.getBean("myCoachR",  RugbyCoach.class);		
		
		//print	
		
		System.out.println("Method test : " + coach.getDailyWorkout());
		System.out.println("Method test : " + coach.getDailyFortune());
		
		//close
		
		context.close();
	}

}
