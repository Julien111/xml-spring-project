package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanScopeDemoApp {

	public static void main(String[] args) {
		//test scope
		
		//load
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanScope-applicationContext.xml");
		
		//retrieve bean container
		BasketballCoach coach = context.getBean("myCoachB",  BasketballCoach.class);
		
		BasketballCoach alphaCoach = context.getBean("myCoachB",  BasketballCoach.class);
		
		boolean result = (coach == alphaCoach);	
		
		//print
		
		System.out.println("pointing object  same object :  " + result);
		System.out.println("Memory location for theCoach : " + coach);
		System.out.println("Memory location for theCoach : " + alphaCoach);
		
		//close
		
		context.close();
	}

}
