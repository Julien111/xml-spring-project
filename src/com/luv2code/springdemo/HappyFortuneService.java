package com.luv2code.springdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HappyFortuneService implements FortuneService {
	
	private static Random randomTest = new Random();

	@Override
	public String getFortune() {		
		
		List<Integer> arrMoney = new ArrayList<>();
		
		for(int i = 0; i < 3; i++) {
			arrMoney.add(getRandomNumberInRange(100, 5000));
		}
		
		int num = randomTest.nextInt((2 - 0) + 1) + 0;
		return "Your Money : " + arrMoney.get(num);
	}
	
	private static int getRandomNumberInRange(int min, int max) {
		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}
		
		return randomTest.nextInt((max - min) + 1) + min;
	}

}
