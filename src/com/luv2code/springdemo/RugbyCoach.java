package com.luv2code.springdemo;

import org.springframework.beans.factory.DisposableBean;

public class RugbyCoach implements Coach, DisposableBean {
	
	private FortuneService fortune;		

	public RugbyCoach() {
		super();
	}	
	
	public RugbyCoach(FortuneService fortune) {
		super();
		this.fortune = fortune;
	}
	
	public void init() {
		System.out.println("Check initialisation.");
	}

	@Override
	public String getDailyWorkout() {		
		return "I do a lot of training";
	}

	@Override
	public String getDailyFortune() {		
		return "Your fortune is : " + fortune.getFortune();
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("Le bean Rugby avec le scope prototype va être détruit.");		
	}

}
