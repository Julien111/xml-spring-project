package com.luv2code.springdemo;

public class SoccerCoach implements Coach {
	
	private FortuneService fortuneService;	
	
	public SoccerCoach() {
		super();		
	}	

	public SoccerCoach(FortuneService fortuneService) {
		super();
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {		
		return "Each team must have eleven players.";
	}

	@Override
	public String getDailyFortune() {		
		return "Soccer in USA ! " + fortuneService.getFortune();
	}
}
