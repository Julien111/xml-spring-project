package com.luv2code.springdemo;

import org.springframework.beans.factory.DisposableBean;

public class TrackCoach implements Coach, DisposableBean {
	
	private FortuneService fortune;		

	public TrackCoach() {
		super();
	}

	public TrackCoach(FortuneService fortuneService) {
		super();
		fortune = fortuneService;
	}

	@Override
	public String getDailyWorkout() {		
		return "Run a hard 5k";
	}
		
	public String getDailyTrack() {		
		return "Specification on TrackCoach";
	}

	@Override
	public String getDailyFortune() {		
		return "Just do it ! " +  fortune.getFortune();
	}
	
	//add init
	
	public void init () {
		System.out.println("Méthode init");
		System.out.println(getDailyTrack());
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("TrackCoach inside, test scope prototype");		
	}
		

}
